A gradle plugin that uses [capabilities](https://docs.gradle.org/6.0-rc-1/userguide/dependency_capability_conflict.html) to check for common SLF4J classpath issues.

# Usage

[Add the plugin](https://plugins.gradle.org/plugin/org.mpierce.slf4j-capabilities) to your build like any other plugin. There's no configuration needed.

At least Gradle 6.0-rc1 is required.

# What this plugin solves

It's pretty common for projects to end up with multiple [SLF4J bindings](http://www.slf4j.org/manual.html) on the classpath, e.g. both `logback-classic` and `slf4j-log4j12`.  Since slf4j just picks one semi arbitrarily, it's an error to have multiple (and it produces a warning message about this at startup.) 

Another problem that can occur is trying to redirect, say, `commons-logging` (aka JCL) API calls into SLF4J (because you don't want to use JCL) by placing `org.slf4j:jcl-over-slf4j` on the classpath, and then having some rude dependency pull in `commons-logging:commons-logging` anyway. Both artifacts would provide the same classes, so you will want to exclude the latter.

Yet another issue is if you try to map, say, log4j calls into SLF4J using `org.slf4j:log4j-over-slf4j` but also place the Log4J binding `org.slf4j:slf4j-log4j12` on the classpath. This would lead to infinite recursion since you don't actually have a real log4j implementation on the classpath.

# I'm getting build errors, now what?

Congrats, the plugin is working as intended, and you've prevented weird logging issues at runtime.

Suppose you're trying to use [Logback](http://logback.qos.ch/) as your logging implementation and route other logging APIs to Logback via SLF4J. The best practice is for libraries to depend only on `slf4j-api`, but frequently the authors don't understand that and mistakenly depend on other things, like maybe a different slf4j binding like `slf4j-log4j12` or legacy like `commons-logging`. 

The solution is to exclude that dependency from everything, everywhere, which you can do with the following (customize the specific module and artifact names as needed):

```kotlin
configurations.all {
    exclude("commons-logging", "commons-logging")
}
```
