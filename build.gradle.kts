plugins {
    kotlin("jvm") version "1.3.50"
    `java-gradle-plugin`
    id("com.gradle.plugin-publish") version "0.10.1"
    id("com.github.ben-manes.versions") version "0.27.0"
    id("net.researchgate.release") version "2.8.1"
}

repositories {
    jcenter()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
}

tasks {
    test {
        useJUnitPlatform()
    }

    afterReleaseBuild {
        dependsOn(publishPlugins)
    }
}

gradlePlugin {
    plugins {
        create("simplePlugin") {
            id = "org.mpierce.slf4j-capabilities"
            implementationClass = "org.mpierce.gradle.plugin.slf4j.capabilities.Slf4JCapabilitiesPlugin"
            displayName = "SLF4J Capabilities Plugin"
            description = "Find SLF4J classpath conflicts at build time using Gradle dependency capabilities"
        }
    }
}

pluginBundle {
    website = "https://bitbucket.org/marshallpierce/gradle-plugin-slf4j-capabilities/src/master/"
    vcsUrl = "https://bitbucket.org/marshallpierce/gradle-plugin-slf4j-capabilities/src/master/"
    tags = listOf("slf4j", "commons-logging", "log4j", "logback", "logging", "classpath")
}
