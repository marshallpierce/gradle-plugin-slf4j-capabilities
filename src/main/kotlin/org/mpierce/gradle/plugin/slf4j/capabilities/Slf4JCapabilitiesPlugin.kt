package org.mpierce.gradle.plugin.slf4j.capabilities

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.ComponentMetadataContext
import org.gradle.api.artifacts.ComponentMetadataRule

class Slf4JCapabilitiesPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        target.dependencies.components.run {
            all(Slf4jBindingCapability::class.java)
            all(CommonsLoggingBridgeCapability::class.java)
            all(Log4jBridgeCapability::class.java)
            all(CommonsLoggingCycleCapability::class.java)
            all(Log4jCycleCapability::class.java)
        }
    }
}

// Can only have one slf4j binding at a time
private val bindingArtifacts = groupByGroupId(listOf(
        Pair("org.slf4j", "slf4j-log4j12"),
        Pair("org.slf4j", "slf4j-jdk14"),
        Pair("org.slf4j", "slf4j-nop"),
        Pair("org.slf4j", "slf4j-simple"),
        Pair("org.slf4j", "slf4j-jcl"),
        Pair("ch.qos.logback", "logback-classic")
))

private class Slf4jBindingCapability : BaseSlf4jCapability(bindingArtifacts, "slf4j-binding")

private val jclArtifacts = groupByGroupId(listOf(
        Pair("org.slf4j", "jcl-over-slf4j"),
        Pair("commons-logging", "commons-logging")
))

private class CommonsLoggingBridgeCapability : BaseSlf4jCapability(jclArtifacts, "commons-logging")

private val log4jArtifacts = groupByGroupId(listOf(
        Pair("org.slf4j", "log4j-over-slf4j"),
        Pair("log4j", "log4j")
))

private class Log4jBridgeCapability : BaseSlf4jCapability(log4jArtifacts, "log4j")

// If you're using slf4j-jcl, you need real commons logging, not the adapter
private val jclCycleArtifacts = groupByGroupId(listOf(
        Pair("org.slf4j", "jcl-over-slf4j"),
        Pair("org.slf4j", "slf4j-jcl")
))

private class CommonsLoggingCycleCapability : BaseSlf4jCapability(jclCycleArtifacts, "commons-logging-cycle")

// If you're using slf4j-log4j12, you need real log4j, not the adapter
private val log4jCycleArtifacts = groupByGroupId(listOf(
        Pair("org.slf4j", "log4j-over-slf4j"),
        Pair("org.slf4j", "slf4j-log4j12")
))

private class Log4jCycleCapability : BaseSlf4jCapability(log4jCycleArtifacts, "log4j-cycle")

private abstract class BaseSlf4jCapability(private val artifacts: Map<String, Set<String>>,
                                           private val capabilityName: String) : ComponentMetadataRule {
    override fun execute(context: ComponentMetadataContext) = context.details.run {
        if (artifacts[id.group]?.contains(id.name) == true) {
            // TODO why do these lambdas need a parameter, but the samples in
            // https://docs.gradle.org/6.0-rc-1/userguide/dependency_capability_conflict.html do not?
            allVariants { v ->
                v.withCapabilities { c ->
                    c.addCapability("org.mpierce.gradle.slf4j-capabilities", capabilityName, id.version)
                }
            }
        }
    }
}

/**
 * Arrange pairs of group/artifact into a map of group -> Set<artifact ids>
 */
private fun groupByGroupId(pairs: Iterable<Pair<String, String>>): Map<String, Set<String>> = pairs
        .groupBy { it.first }
        .mapValues { (_, pairs) -> pairs.map { it.second }.toSet() }
