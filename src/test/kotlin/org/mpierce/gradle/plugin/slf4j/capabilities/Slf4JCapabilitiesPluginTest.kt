package org.mpierce.gradle.plugin.slf4j.capabilities

import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome.FAILED
import org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path


internal class Slf4JCapabilitiesPluginTest {
    @TempDir
    lateinit var testProjectDir: Path
    private lateinit var buildFile: Path

    @BeforeEach
    fun setup() {
        buildFile = testProjectDir.resolve("build.gradle.kts")
    }

    @Test
    internal fun worksWithValidSetup() {
        writeSource()
        buildFile.write("""
            $prologue
            
            dependencies {
                implementation("org.slf4j:slf4j-api:1.7.28")
                implementation("org.slf4j:jcl-over-slf4j:1.7.28")
                implementation("org.slf4j:log4j-over-slf4j:1.7.28")
                implementation("org.slf4j:jul-to-slf4j:1.7.28")
                implementation("ch.qos.logback:logback-classic:1.2.3")
            }""")

        val result = prepareBuild()
                .build()

        assertTrue(result.output.contains("BUILD SUCCESSFUL"), result.output)
        assertEquals(UP_TO_DATE, result.task(":check")!!.outcome)
    }

    @Test
    internal fun detectsAdapterConflict() {
        writeSource()
        buildFile.write("""
            $prologue
            
            dependencies {
                implementation("org.slf4j:slf4j-api:1.7.28")
                
                // should conflict
                implementation("org.slf4j:slf4j-simple:1.7.28")
                implementation("ch.qos.logback:logback-classic:1.2.3")
            }""")

        val result = prepareBuild()
                .buildAndFail()

        assertEquals(setOf(
                "> Module 'org.slf4j:slf4j-simple' has been rejected:",
                "> Module 'ch.qos.logback:logback-classic' has been rejected:"),
                result.output.rejectedModuleLines())
        assertEquals(setOf(
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:slf4j-binding:1.7.28' also provided by [ch.qos.logback:logback-classic:1.2.3(compile)]",
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:slf4j-binding:1.2.3' also provided by [org.slf4j:slf4j-simple:1.7.28(compile)]"),
                result.output.rejectedModuleCapabilityLines())
        assertTrue(result.output.contains("BUILD FAILED"), result.output)
        assertEquals(FAILED, result.task(":compileJava")!!.outcome)
    }

    @Test
    internal fun detectsJclConflict() {
        writeSource()
        buildFile.write("""
            $prologue
            
            dependencies {
                implementation("org.slf4j:slf4j-api:1.7.28")
                
                // should conflict
                implementation("org.slf4j:jcl-over-slf4j:1.7.28")
                implementation("commons-logging:commons-logging:1.2")
            }""")

        val result = prepareBuild()
                .buildAndFail()

        assertEquals(setOf(
                "> Module 'org.slf4j:jcl-over-slf4j' has been rejected:",
                "> Module 'commons-logging:commons-logging' has been rejected:"),
                result.output.rejectedModuleLines())
        assertEquals(setOf(
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:commons-logging:1.7.28' also provided by [commons-logging:commons-logging:1.2(compile)]",
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:commons-logging:1.2' also provided by [org.slf4j:jcl-over-slf4j:1.7.28(compile)]"),
                result.output.rejectedModuleCapabilityLines())
        assertTrue(result.output.contains("BUILD FAILED"), result.output)
        assertEquals(FAILED, result.task(":compileJava")!!.outcome)
    }

    @Test
    internal fun detectsLog4jConflict() {
        writeSource()
        buildFile.write("""
            $prologue
            
            dependencies {
                implementation("org.slf4j:slf4j-api:1.7.28")
                
                // should conflict
                implementation("org.slf4j:log4j-over-slf4j:1.7.28")
                implementation("log4j:log4j:1.2.17")
            }""")

        val result = prepareBuild()
                .buildAndFail()

        assertEquals(setOf(
                "> Module 'org.slf4j:log4j-over-slf4j' has been rejected:",
                "> Module 'log4j:log4j' has been rejected:"),
                result.output.rejectedModuleLines())
        assertEquals(setOf(
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:log4j:1.7.28' also provided by [log4j:log4j:1.2.17(compile)]",
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:log4j:1.2.17' also provided by [org.slf4j:log4j-over-slf4j:1.7.28(compile)]"),
                result.output.rejectedModuleCapabilityLines())
        assertTrue(result.output.contains("BUILD FAILED"), result.output)
        assertEquals(FAILED, result.task(":compileJava")!!.outcome)
    }

    @Test
    internal fun detectsJclCycle() {
        writeSource()
        buildFile.write("""
            $prologue
            
            dependencies {
                implementation("org.slf4j:slf4j-api:1.7.28")
                
                // should conflict
                implementation("org.slf4j:jcl-over-slf4j:1.7.28")
                implementation("org.slf4j:slf4j-jcl:1.7.28")
            }""")

        val result = prepareBuild()
                .buildAndFail()

        assertEquals(setOf(
                "> Module 'org.slf4j:jcl-over-slf4j' has been rejected:",
                "> Module 'org.slf4j:slf4j-jcl' has been rejected:",
                "> Module 'commons-logging:commons-logging' has been rejected:"
                ),
                result.output.rejectedModuleLines())
        assertEquals(setOf(
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:commons-logging:1.7.28' also provided by [commons-logging:commons-logging:1.1.1(compile)]",
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:commons-logging-cycle:1.7.28' also provided by [org.slf4j:jcl-over-slf4j:1.7.28(compile)]",
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:commons-logging:1.1.1' also provided by [org.slf4j:jcl-over-slf4j:1.7.28(compile)]"),
                result.output.rejectedModuleCapabilityLines())
        assertTrue(result.output.contains("BUILD FAILED"), result.output)
        assertEquals(FAILED, result.task(":compileJava")!!.outcome)
    }

    @Test
    internal fun detectsLog4jCycle() {
        writeSource()
        buildFile.write("""
            $prologue
            
            dependencies {
                implementation("org.slf4j:slf4j-api:1.7.28")
                
                // should conflict
                implementation("org.slf4j:log4j-over-slf4j:1.7.28")
                implementation("org.slf4j:slf4j-log4j12:1.7.28")
            }""")

        val result = prepareBuild()
                .buildAndFail()

        assertEquals(setOf(
                "> Module 'org.slf4j:log4j-over-slf4j' has been rejected:",
                "> Module 'org.slf4j:slf4j-log4j12' has been rejected:",
                "> Module 'log4j:log4j' has been rejected:"
        ),
                result.output.rejectedModuleLines())
        assertEquals(setOf(
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:log4j:1.7.28' also provided by [log4j:log4j:1.2.17(compile)]",
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:log4j-cycle:1.7.28' also provided by [org.slf4j:log4j-over-slf4j:1.7.28(compile)]",
                "Cannot select module with conflict on capability 'org.mpierce.gradle.slf4j-capabilities:log4j:1.2.17' also provided by [org.slf4j:log4j-over-slf4j:1.7.28(compile)]"),
                result.output.rejectedModuleCapabilityLines())
        assertTrue(result.output.contains("BUILD FAILED"), result.output)
        assertEquals(FAILED, result.task(":compileJava")!!.outcome)
    }

    private fun prepareBuild(): GradleRunner {
        return GradleRunner.create()
                .withProjectDir(testProjectDir.toFile())
                .withPluginClasspath()
                .withArguments("check")
                .withDebug(true)
    }

    /**
     * Has to be some source to be compiled or dep resolution won't happen
     */
    private fun writeSource() {
        val srcDir = testProjectDir.resolve("src/main/java")
        check(srcDir.toFile().mkdirs())

        srcDir.resolve("Foo.java").write("""
            class Foo {
                public static void main(String[] args) { System.out.println("Hello world"); }
            }
        """.trimIndent())

    }
}

private fun Path.write(s: String) = Files.newBufferedWriter(this, StandardCharsets.UTF_8).use {
    it.write(s)
}

private fun String.rejectedModuleLines(): Set<String> =
        this
                .lineSequence()
                .map { it.trim() }
                .filter { it.endsWith("has been rejected:") }
                .toSet()

private fun String.rejectedModuleCapabilityLines(): Set<String> =
        this
                .lineSequence()
                .map { it.trim() }
                .filter { it.startsWith("Cannot select module with conflict on capability") }
                .toSet()

val prologue = """
    plugins { 
        java
        id("org.mpierce.slf4j-capabilities") 
    }
    
    repositories {
        jcenter()
    }
""".trimIndent()
